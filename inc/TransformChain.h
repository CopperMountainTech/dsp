#pragma once
#include "DspGlobal.h"
#include "AbstractVector.h"
#include "ComplexVector.h"
#include "AbstractTransform.h"
#include <QThreadPool>

namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс цепочки преобразований.
/// \details Цепочка преобразований должна состоять из чередующихся векторов данных и объектов
/// преобразований. Минимально возможная для запуска цепочка должна состоять из 1 преобразования
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT TransformChain : public AbstractTransform<T>
{
    typedef AbstractTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    ///
    TransformChain(): _Parent(0)
    {

    }
    ///
    /// \brief Деструктор
    ///
    ~TransformChain()
    {

    }
    ///
    /// \brief Добавляет в цепочку вектор.
    /// \param[in] vector указатель на вектор
    /// \param[in] offset смещение в векторе от начала
    ///
    void push_back(AbstractVector* vector, const std::size_t offset = 0);
    ///
    /// \brief Добавляет в цепочку преобразование
    /// \param[in] transform ссылка на преобразование
    ///
    void push_back(AbstractTransform<T>& transform);
    ///
    /// \brief Задаёт тип преобразования
    /// \param[in] newType новый тип преобразования
    ///
    void setType(TransformType newType)
    {
        _type = newType;
    }

    std::size_t getBytesCount() const;
protected:

    void runChain(std::vector<SubVector> data, const std::size_t num = 0);
    void runAbstract(AbstractVector* source, AbstractVector* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0)
    {
        runAbstract(source, result, num, sourceOffset, resultOffset);
    }
    virtual void run(ComplexVector<T>* source, RealVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0)
    {
        runAbstract(source, result, num, sourceOffset, resultOffset);
    }
    virtual void run(RealVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0)
    {
        runAbstract(source, result, num, sourceOffset, resultOffset);
    }
    virtual void run(RealVector<T>* source, RealVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0)
    {
        runAbstract(source, result, num, sourceOffset, resultOffset);
    }

    std::vector<SubVector> _data;
    std::vector<AbstractTransform<T>* > _transforms;
    using _Parent::_type;
};

///
/// \brief Класс цепочки преобразований одинарной точности
/// \ingroup Transforms
///
typedef TransformChain<float> TransformChain32;
///
/// \brief Класс цепочки преобразований двойной точности
/// \ingroup Transforms
///
typedef TransformChain<double> TransformChain64;

}
}
