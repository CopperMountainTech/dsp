#pragma once
#include "DspGlobal.h"
#include "ComplexVector.h"
#include "RealVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс циклических векторов
/// \ingroup Vectors
///
template<class Base>
class DSPSHARED_EXPORT Cyclic : public Base
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    Cyclic(const std::size_t size): Base(2 * size - 1)
    {
    }

    ///
    /// \brief Получение интерфейса обычного вектора
    /// \return указатель на интерфейс
    ///
    Base* getBase()
    {
        return this;
    }

    virtual std::size_t getSize() const
    {
        return (_size + 1) / 2;
    }
    virtual void resize(const std::size_t size)
    {
        Base::resize(2 * size - 1);
    }

    ///
    /// \brief Сдвигает индекс вектора вперед или назад
    /// \param index исходное значение индекса
    /// \param offset величина сдвига. минус говорит о том, что сдвиг делается назад
    /// \return итоговое значение индекса
    ///
    std::size_t moveIndex(std::size_t index, int offset);

protected:
    virtual std::size_t getSizeFromIdxToEnd(const std::size_t /*idx*/) const
    {
        return this->getSize();
    }
    virtual void doCustomProcessing(const std::size_t startIdx = 0,
                                    const std::size_t length = 0);

    using Base::_samples;
    using Base::_size;
};

///
/// \brief Циклический вектор комплексных чисел одинарной точности
/// \ingroup Vectors
///
typedef Cyclic<ComplexVector32> CyclicComplexVector32;
///
/// \brief Циклический вектор комплексных чисел двойной точности
/// \ingroup Vectors
///
typedef Cyclic<ComplexVector64> CyclicComplexVector64;
///
/// \brief Циклический вектор вещественных чисел одинарной точности
/// \ingroup Vectors
///
typedef Cyclic<RealVector32> CyclicRealVector32;
///
/// \brief Циклический вектор вещественных чисел двойной точности
/// \ingroup Vectors
///
typedef Cyclic<RealVector64> CyclicRealVector64;

}
}
