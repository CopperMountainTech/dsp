#pragma once
#include "DspGlobal.h"
#include "AbstractTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс сглаживания
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT SmoothTransform : public AbstractTransform<T>
{
    typedef AbstractTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор по умолчанию - создаёт неинициализированный сглаживатель.
    /// Для инициализации нужно задать фильтр с помощью setTaps()
    ///
    SmoothTransform();
    ///
    /// \brief Деструктор
    ///
    ~SmoothTransform();

    std::size_t getBytesCount() const override;

    ///
    /// \brief Инициализация сглаживателя фильтром
    /// \param[in] taps вектор коэффициентов фильтра. Для генерации можно использовать FilterBank
    ///
    void setTaps(const RealVector<T>& taps);

protected:
    void run(RealVector<T>* source, RealVector<T>* result,
             const std::size_t num, const std::size_t sourceOffset, const std::size_t resultOffset) override;
    void run(ComplexVector<T>* source, ComplexVector<T>* result,
             const std::size_t num, const std::size_t sourceOffset, const std::size_t resultOffset) override;

    using _Parent::_size;
    using _Parent::_context;
    using _Parent::_type;
    RealVector<T> _tapsSums;
};

///
/// \brief Класс сглаживания одинарной точности
/// \ingroup Transforms
///
typedef SmoothTransform<float> SmoothTransform32;
///
/// \brief Класс сглаживания двойной точности
/// \ingroup Transforms
///
typedef SmoothTransform<double> SmoothTransform64;

} // namespace Dsp
} // namespace Planar
