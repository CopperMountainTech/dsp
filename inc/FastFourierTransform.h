#pragma once
#include "DspGlobal.h"
#include "FourierTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Базовый шаблонный класс БПФ
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT FastFourierTransform : public FourierTransform<T>
{
    typedef FourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    /// \param[in] order порядок степени числа 2
    ///
    FastFourierTransform(int order);
    ~FastFourierTransform();

    virtual AbstractTransform<T>& operator()(ComplexVector<T>& source, ComplexVector<T>& result,
                                     const int sourceOffset = 0, const int resultOffset = 0) = 0;

    std::size_t getBytesCount() const;
    ///
    /// \brief Возвращает порядок степени 2
    /// \return порядок степени 2
    ///
    inline int getOrder() const
    {
        return _order;
    }
protected:
    using _Parent::_context;
    int _order;
};

typedef FastFourierTransform<float> FastFourierTransform32;
typedef FastFourierTransform<double> FastFourierTransform64;

}
}
