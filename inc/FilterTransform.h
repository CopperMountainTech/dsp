#pragma once
#include "DspGlobal.h"
#include "AbstractTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс фильтрации
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT FilterTransform : public AbstractTransform<T>
{
    typedef AbstractTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    /// \param[in] taps вектор хранящий в себе значения фильтров
    /// \param[in] downFactor коэффициент понижения дискретизации
    /// \param[in] upFactor коэффициент повышения дискретизации
    ///
    FilterTransform(const ComplexVector<T>* taps, int downFactor = 1, int upFactor = 1);
    ~FilterTransform();
    ///
    /// \brief Задать входную линию задержки
    /// \param[in] sourceDelayLine вектор входной линии задержки
    ///
    void setSourceDelayLine(ComplexVector<T>* sourceDelayLine);
    ///
    /// \brief Задать выходную линию задержки
    /// \param[out] destinationDelayLine вектор выходной линии задержки
    ///
    void setDestinationDelayLine(ComplexVector<T>* destinationDelayLine);
    ///
    /// \brief Расчитывает необходимую длину линий задержки
    /// \return длина
    ///
    std::size_t getDelayLineSize() const;
    ///
    /// \brief Меняет входную и выходную линии задержки местами
    ///
    void swapDelayLines();
    ///
    /// \brief Выделяет память для линий задержки и заполняет их нулями
    ///
    void allocateDelayLines();

    std::size_t getBytesCount() const;

    ///
    /// \brief Расчитывает размер необходимого объёма памяти для некоторого количества фильтраций.
    /// \param[in] tapsLength длина фильтра
    /// \param[in] downFactor коэффициент прореживания
    /// \param[in] upFactor коэффициент повышения частоты
    /// \param[in] appFactor коэффициент применимости - количество однотипных преобразований
    /// \return размер памяти в байтах
    ///
    static std::size_t calcNeededMemory(std::size_t tapsLength, int downFactor, int upFactor,
                                        unsigned int appFactor);

protected:

    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
    using _Parent::_size;
    using _Parent::_context;
    using _Parent::_type;
};

///
/// \brief Класс фильтрации одинарной точности
/// \ingroup Transforms
///
typedef FilterTransform<float> FilterTransform32;
///
/// \brief Класс фильтрации двойной точности
/// \ingroup Transforms
///
typedef FilterTransform<double> FilterTransform64;

}
}
