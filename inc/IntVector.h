#pragma once
#include "DspGlobal.h"
#include "AbstractSampleVector.h"


namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс векторов целых чисел
/// \ingroup Vectors
///
template<class T>
class DSPSHARED_EXPORT IntVector : public AbstractSampleVector<T>
{
    typedef AbstractSampleVector<T> BaseType;
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    IntVector(const std::size_t size);
    ///
    /// \brief Конструктор с инициализацией
    /// \param[in] size размер вектора
    /// \param[in] initValue значение для инициализации
    ///
    IntVector(const std::size_t size, const T& initValue);
    ///
    /// \brief Деструктор
    ///
    virtual ~IntVector();

    ///
    /// \brief Побитовый сдвиг всех элементов влево.
    /// Реализовано только для ShortVector
    /// \param[in] shiftVal количество бит для сдвига
    /// \return ссылка на вектор
    ///
    IntVector<T>& operator<<(int shiftVal);
    ///
    /// \brief Побитовый сдвиг всех элементов вправо.
    /// Реализовано только для ShortVector
    /// \param[in] shiftVal количество бит для сдвига
    /// \return ссылка на вектор
    ///
    IntVector<T>& operator>>(int shiftVal);

protected:
    virtual QString toString(std::size_t idx) const;
    virtual int symbolsInTextValue() const;

    using AbstractSampleVector<T>::_samples;
    using AbstractSampleVector<T>::_type;
};

}
}
