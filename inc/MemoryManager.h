#pragma once
#include "DspGlobal.h"
#include <QMutex>
#include <QMutexLocker>
#include <AbstractInstance.h>
#include <AbstractVector.h>
#include <set>

namespace Planar {
namespace Dsp {

///
/// \brief Синглтон-класс менеджер памяти
/// \ingroup Essentials
///
class DSPSHARED_EXPORT MemoryManager
{
public:
    ///
    /// \brief Метод получения экземпляра менеджера
    /// \return указатель на объект менеджера
    ///
    static MemoryManager* getInstance()
    {
        if (!MemoryManager::_instance)
            MemoryManager::_instance = new MemoryManager;
        return MemoryManager::_instance;
    }
    ///
    /// \brief Возвращает размер выделенной памяти
    /// \return размер в байтах
    ///
    long long getDataSize()
    {
        QMutexLocker lock(&_mutex);
        return _allocatedSize;
    }
    ///
    /// \brief Возвращает количество инстанцированных векторов
    /// \return количество векторов
    ///
    int getVectorCount()
    {
        QMutexLocker lock(&_mutex);
        return int(_vectors.size());
    }
    ///
    /// \brief Возвращает количество инстанцированных преобразований
    /// \return количество объектов
    ///
    int getTransformCount()
    {
        QMutexLocker lock(&_mutex);
        return int(_transforms.size());
    }

    ///
    /// \brief Добавляет вектор в базу менеджера
    /// \param vector указатель на вектор
    ///
    void addVector(AbstractVector* vector)
    {
        QMutexLocker lock(&_mutex);
        _allocatedSize += vector->getBytesCount();
        _vectors.insert(vector);
    }
    ///
    /// \brief Удаляет вектор из базы менеджера
    /// \param vector указатель на вектор
    ///
    void removeVector(AbstractVector* vector)
    {
        QMutexLocker lock(&_mutex);
        _allocatedSize -= vector->getBytesCount();
        _vectors.erase(vector);
    }

    ///
    /// \brief Добавляет преобразование в базу менеджера
    /// \param transform указатель на объект преобразования
    ///
    void addTransfrom(AbstractInstance* transform)
    {
        QMutexLocker lock(&_mutex);
        _allocatedSize += transform->getBytesCount();
        _transforms.insert(transform);
    }
    ///
    /// \brief Удаляет преобразование из базы менеджера
    /// \param transform указатель на объект преобразования
    ///
    void removeTransfrom(AbstractInstance* transform)
    {
        QMutexLocker lock(&_mutex);
        _allocatedSize -= transform->getBytesCount();
        _transforms.erase(transform);
    }

    ///
    /// \brief Возвращает список инстанцированных векторов
    ///
    QList<AbstractVector*> getVectorList();

    ///
    /// \brief Возвращает список инстанцированных преобразований
    ///
    QList<AbstractInstance*> getTransformList();

private:
    ///
    /// \brief Приватный конструктор
    ///
    MemoryManager():
        _allocatedSize(0), _memoryLimit(4000000000)
    {
    }

    static MemoryManager* _instance;
    QMutex _mutex;
    std::size_t _allocatedSize;
    std::set<AbstractVector*> _vectors;
    std::set<AbstractInstance*> _transforms;
    std::size_t _memoryLimit;
};

}
}
