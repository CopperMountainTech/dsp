#pragma once

#include <QString>

#include "DspGlobal.h"

namespace Planar {
namespace Dsp {

const QString LIB_VERSION = "0.1.0";

/// @defgroup Essentials Прочие полезные функции
/// Прочие полезные функции и классы
/// @defgroup Vectors Классы векторов
/// Классы векторов
/// @defgroup Markups Классы разметок
/// @ingroup Vectors
/// Классы горизонтальной разметки для векторов
/// @defgroup Transforms Классы преобразований
/// Классы преобразований векторов


///
/// \brief Возвращает версию библиотеки planar.dsp
/// \return тектстовая строка с версией библиотеки planar.dsp
/// \ingroup Essentials
/// \param[in] full если false, то будет выведена только версия самой DSP-библиотеки.
/// если true то будет так же выведена версия TP библиотеки.
///
QString DSPSHARED_EXPORT getLibVersion(bool full = true);
///
/// \brief Возвращает версию загруженной Third-Party библиотеки
/// \return тектстовая строка с версией загруженной Third-Party библиотеки
/// \ingroup Essentials
///
QString DSPSHARED_EXPORT get3PLoadedVersion();
///
/// \brief Возвращает версию Third-Party библиотеки, для которой была откомпилирована библиотека planar.dsp
/// \return тектстовая строка с версией Third-Party библиотеки, с которой была откомпилирована библиотека planar.dsp
/// \ingroup Essentials
///
QString DSPSHARED_EXPORT get3PCompiledVersion();

///
/// \brief Возвращает текущее количество тиков центрального процессора.
/// Нужна для оценки времени выполнения какого-то участка кода.
/// \return количество тиков
///
long long DSPSHARED_EXPORT getCpuClocks();
}
}
