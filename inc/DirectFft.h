#pragma once
#include "DspGlobal.h"
#include "FastFourierTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс прямого БПФ по основанию 2^n
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT DirectFft : public FastFourierTransform<T>
{
    typedef FastFourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструтор
    /// \param[in] order порядок степени 2
    ///
    DirectFft(int order): _Parent(order) { }

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    using _Parent::_context;
};

///
/// \brief Класс прямого БПФ одинарной точности
/// \ingroup Transforms
///
typedef DirectFft<float> DirectFft32;
///
/// \brief Класс прямого БПФ двойной точности
/// \ingroup Transforms
///
typedef DirectFft<double> DirectFft64;

}
}
