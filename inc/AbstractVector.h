#pragma once
#include "DspGlobal.h"
#include "AbstractInstance.h"
#include "Abscissa.h"
#include "Complex.h"
#include <Units.h>

namespace Planar {
namespace Dsp {

///
/// \brief Точности векторов
/// \ingroup Vectors
///
enum PrecisionType {
    /// одинарная точноть
    SinglePrecision = 0,
    /// двойная точность
    DoublePrecision
};
///
/// \brief Типы векторов
/// \ingroup Vectors
///
enum VectorType {
    /// вектор вещественных чисел
    RealVectorType = 0,
    /// вектор комплексных чисел
    ComplexVectorType,
    /// вектор целых чисел
    IntVectorType
};

///
/// \brief Абстрактный интерфейсный класс векторов
/// \ingroup Vectors
///
class DSPSHARED_EXPORT AbstractVector: public AbstractInstance
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    AbstractVector(const std::size_t size);
    ///
    /// \brief Деструктор
    ///
    virtual ~AbstractVector();

    ///
    /// \brief Копирует кусок вектора из одного места в другое.
    /// Если какие-то части исходной и итоговой областей пересекаются, то
    /// функция сначала копирует область пересечения прежде чем она будет
    /// перезаписана.
    /// \param[in] srcOffset исходное смещение
    /// \param[in] dstOffset итоговое смещение
    /// \param[in] len размер копируемой области
    ///
    virtual void copy(const std::size_t srcOffset, const std::size_t dstOffset,
                      const std::size_t len) = 0;
    ///
    /// \brief Сдвигает все элементы вектора на определённое количество элементов вперед или назад.
    /// Появившиеся новые элементы заполняются 0.
    /// \param[in] count количетсво элементов, на которое нужно сдвинуть. Если >0, то сдвиг в сторону
    /// увеличения индексов, иначе в сторону уменьшения
    ///
    void shiftElements(int count);

    ///
    /// \brief Копирует значения другого вектороа в текущий
    /// \param[in] anotherVector вектор целых 2байтовых чисел
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] dstOffset смещение в конечном векторе
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца исходного вектора
    ///
    virtual void copyFrom(const AbstractVector* anotherVector,
                          const std::size_t srcOffset = 0, const std::size_t dstOffset = 0,
                          const std::size_t count = 0) = 0;

    ///
    /// \brief Возвращает количество байт для одного элемента
    /// \return количество байт
    ///
    virtual std::size_t getBytesPerSample() const = 0;
    virtual std::size_t getBytesCount() const;
    ///
    /// \brief Изменяет размер вектора, при этом разметка удаляется, если она была
    /// \param[in] size новый размер
    ///
    virtual void resize(const std::size_t size);
    ///
    /// \brief Возвращает тип вектора
    /// \return тип
    ///
    VectorType getType() const;
    ///
    /// \brief Возвращает точность вектора.
    /// Актуально только для вещественных и комплексных векторов
    /// \return точность
    ///
    PrecisionType getPrecision() const;
    ///
    /// \brief Заполняет область вектора нулями (со значениями параметров по-умолчанию - весь вектор)
    /// \param[in] offset смещение начала области относительно начала вектора
    /// \param[in] count количество элементов в области. Если 0, то до конца
    ///
    virtual void fillZeroes(std::size_t offset = 0, std::size_t count = 0) = 0;
    ///
    /// \brief Разметка вектора
    /// \return указатель на объект разметки
    ///
    AbstractAbscissa* getAbscissa() const;
    ///
    /// \brief Устанавливает разметку вектора
    /// \param[in] abs указатель на объект разметки
    ///
    void setAbscissa(AbstractAbscissa* abs);
    ///
    /// \brief Копирует разметку из другого объекта разметки
    /// \param[in] abs указатель на объект разметки
    ///
    void copyAbscissaFrom(const AbstractAbscissa* abs);
    ///
    /// \brief Удаляет разметку вектора
    ///
    void deleteAbscissa();
    ///
    /// \brief Задает произвольную разметку по горизонтальной оси
    /// \details Необходимо явно задать значение для каждой точки
    ///
    void setAbscissaRnd();
    ///
    /// \brief Задаёт простую разметку по горизонтальной оси
    ///
    void setAbscissaSimple();
    ///
    /// \brief Задает равномерную разметку по горизонтальной оси по мин. и макс значениям
    /// \param[in] xMin минимальное значение
    /// \param[in] xMax максимальное значение
    /// \param[in] lastSample флаг включения в разметку максимального значения
    ///
    void setAbscissaUniByRange(const double xMin, const double xMax, const bool lastSample = false);
    ///
    /// \brief Задает равномерную разметку по горизонтальной оси
    /// \param[in] x0 начальное значение
    /// \param[in] step шаг
    ///
    void setAbscissaUniByStep(const double x0, const double step);
    ///
    /// \brief Сохранить значения вектора в файл
    /// \param[in] fileName имя файла
    ///
    virtual void saveToFile(const QString& fileName) const = 0;
    ///
    /// \brief Загрузить значения вектора из файла формата fcf (MathLab FDATool)
    /// \param[in] fileName имя файла
    ///
    virtual void loadFromfcfFile(const QString& fileName) = 0;
    ///
    /// \brief Загрузить значения вектора из файла
    /// \param[in] fileName имя файла
    ///
    virtual void loadFromFile(const QString& fileName) = 0;

    virtual std::size_t getSizeFromIdxToEnd(const std::size_t idx) const;

    ///
    /// \brief Единицы измерения элементов вектора
    /// \return объект Unit
    ///
    Unit scaleUnit() const;

    ///
    /// \brief Задаёт единицы измерения элементов вектора
    /// \param[in] valueUnit объект Unit
    /// \param[in] rescaleRequired если true, то значения элементов вектора
    /// изменятся согласно новым единицам измерений
    ///
    virtual void setScaleUnit(const Unit& valueUnit, bool rescaleRequired = false) = 0;

    ///
    /// \brief Формирует побайтовое текстовое представление вектора
    /// \param[in] delimeter символ-разделитель между элементами
    /// \return объект QByteArray
    ///
    virtual QByteArray toQByteArray(char delimeter = ',') const = 0;
    ///
    /// \brief Формирует побайтовое бинарное представление вектора
    /// \param[in] isInverseOrder если true, то байты в элементе будут идти в обратной последовательности
    /// \return объект QByteArray
    ///
    virtual QByteArray toBinaryQByteArray(bool isInverseOrder = false) const = 0;

    ///
    /// \brief Проверка на вещественный вектор
    /// \return true если вектор вещественных чисел
    ///
    bool isReal() const;
    ///
    /// \brief Проверка на комплексный вектор
    /// \return true если вектор комплексных чисел
    ///
    bool isComplex() const;
    ///
    /// \brief Проверка на целочисленный вектор
    /// \return true если вектор целых чисел
    ///
    bool isInt() const;
    ///
    /// \brief Проверка на двойную точность
    /// \return true если вектор имеет двойную точность
    ///
    bool hasDoublePrecision() const;
    ///
    /// \brief Проверка на нулевой элемент вектора
    /// \param[in] index индекс элементы
    /// \return  true если элемент нулевой
    ///
    virtual bool isZero(const std::size_t index) const = 0;
    ///
    /// \brief Комплексный геттер элемента вектора
    /// \param[in] index индекс элементы
    /// \return значение элемента в формате Complex64
    ///
    virtual Complex64 complexAt(const std::size_t index) const = 0;
protected:

    AbstractAbscissa* _abscissa;
    VectorType _type;
    PrecisionType _precision;
    Unit _valueUnit;
};

///
/// \brief Структура описывает часть вектора
///
struct SubVector {
    ///
    /// \brief Конструктор
    /// \param vector указатель на вектор
    /// \param offset смещение внутри вектора
    ///
    SubVector(AbstractVector* vector, std::size_t offset = 0):
        Vector(vector), Offset(offset)
    {

    }
    ///
    /// \brief Интерфейсный указатель на вектор
    ///
    AbstractVector* Vector;
    ///
    /// \brief Смещение относительно начала вектора
    ///
    std::size_t Offset;
};

}
}
