#pragma once
#include "DspGlobal.h"
#include "AbstractInstance.h"
#include "AbstractVector.h"
#include "ComplexVector.h"
#include "RealVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Битовое множество типов преобразований
///
enum TransformType {
    /// Неопределенный тип
    NONE = 0,
    /// Преобразование является In-place преобразованием, т.е. результат перезаписывает исходные данные
    INPLACE = 1,
    /// Преобразование ComplexVector -> ComplexVector
    C2C = 2,
    /// Преобразование ComplexVector -> RealVector
    C2R = 4,
    /// Преобразование RealVector -> ComplexVector
    R2C = 8,
    /// Преобразование RealVector -> RealVector
    R2R = 16
};

///
/// \brief Базовый шаблонный класс преобразований
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT AbstractTransform: public AbstractInstance
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер преобразования
    ///
    AbstractTransform(std::size_t size);

    ///
    /// \brief Возвращает тип преобразования
    /// \return битовое поле - тип преобразования
    ///
    int getType()
    {
        return _type;
    }

    ///
    /// \brief Деструктор
    ///
    virtual ~AbstractTransform()
    {
    }

    ///
    /// \brief Выполняет преобразование
    /// \param[in] source указатель на исходный вектор
    /// \param[out] result указатель на результирующий вектор
    /// \param[in] num количество элементов из исходного вектора для преобразования
    /// \param[in] sourceOffset смещение в исходном векторе
    /// \param[in] resultOffset смещение в результирующем векторе
    /// \return ссылка на преобразование
    ///
    virtual AbstractTransform<T>& operator()(AbstractVector* source, AbstractVector* result,
                                             const std::size_t num = 0,
                                             const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    ///
    /// \brief Оценивает время работы преобразования
    /// \param[in] source указатель на исходный вектор
    /// \param[out] result указатель на результирующий вектор
    /// \param[in] num количество элементов из исходного вектора для преобразования
    /// \param[in] sourceOffset смещение в исходном векторе
    /// \param[in] resultOffset смещение в результирующем векторе
    /// \return затраченное на преобразование количество тактов
    ///
    long long runPerformanceTest(AbstractVector* source, AbstractVector* result,
                                 const std::size_t num = 0,
                                 const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

protected:
    ///
    /// \brief контекст преобразования
    ///
    void* _context;
    ///
    /// \brief (битовое поле) тип преобразования: вектора каких типов может принимать и возвращать.
    ///
    int _type;

    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
    virtual void run(RealVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
    virtual void run(ComplexVector<T>* source, RealVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
    virtual void run(RealVector<T>* source, RealVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);
};

typedef AbstractTransform<float> Transform32;
typedef AbstractTransform<double> Transform64;
}
}
