#pragma once
#include "AbstractVector.h"
#include "Complex.h"
#include <QString>

namespace Planar {
namespace Dsp {

///
/// \brief Абстрактный вектор сэмплов
/// \details Базовый шаблонный класс для целых, вещественных, комплексных векторов.
/// \ingroup Vectors
///
template<class T>
class DSPSHARED_EXPORT AbstractSampleVector: public AbstractVector
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    AbstractSampleVector(const std::size_t size);
    ///
    /// \brief Конструктор с инициализацией
    /// \param[in] size размер вектора
    /// \param[in] initValue значение для инициализации
    ///
    AbstractSampleVector(const std::size_t size, const T& initValue);
    ///
    /// \brief Деструктор
    ///
    virtual ~AbstractSampleVector();

    ///
    /// \brief Заполняет область вектора значением
    /// \param[in] value значение
    /// \param[in] offset смещение области внутри вектора
    /// \param[in] len длинна области заполнения, если 0, то до конца вектора
    ///
    void assign(const T& value, const std::size_t offset = 0, const std::size_t len = 0);
    virtual void resize(const std::size_t size);
    std::size_t getBytesPerSample() const;
    virtual void copy(const std::size_t srcOffset, const std::size_t dstOffset, const std::size_t len);


    ///
    /// \brief Предоставляет доступ к указанному элементу с проверкой индекса
    /// \param[in] index индекс элемента вектора
    /// \return ссылка на требуемый элемент вектора
    ///
    T& at(const std::size_t index);
    ///
    /// \brief Предоставляет  константный доступ к указанному элементу с проверкой индекса
    /// \param[in] index индекс элемента вектора
    /// \return ссылка на требуемый элемент вектора
    ///
    const T& at(const std::size_t index) const;
    ///
    /// \brief Предоставляет доступ к указанному элементу
    /// \param[in] index индекс элемента вектора
    /// \return ссылка на требуемый элемент вектора
    ///
    T& operator[](const std::size_t index);
    ///
    /// \brief Предоставляет константный доступ к указанному элементу
    /// \param[in] index индекс элемента вектора
    /// \return константная ссылка на требуемый элемент вектора
    ///
    const T& operator[](const std::size_t index) const;

    virtual void copyFrom(const AbstractVector* anotherVector,
                          const std::size_t srcOffset = 0, const std::size_t dstOffset = 0,
                          const std::size_t count = 0) override;

    virtual void saveToFile(const QString& fileName) const;
    virtual void loadFromfcfFile(const QString& fileName);
    virtual void loadFromFile(const QString& fileName);

    ///
    /// \brief Поэлементное суммирование со значениями другого вектора
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator+=(const AbstractSampleVector<T>& anotherVector);
    ///
    /// \brief Увеличивает каждое значение вектора на заданное значение
    /// \param[in] singleValue заданное значение
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator+=(const T& singleValue);
    ///
    /// \brief Поэлементное вычитание значений другого вектора
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator-=(const AbstractSampleVector<T>& anotherVector);
    ///
    /// \brief Уменьшает каждое значение вектора на заданное значение
    /// \param[in] singleValue заданное значение
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator-=(const T& singleValue);
    ///
    /// \brief Поэлементное умножение с элементами другого вектора
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator*=(const AbstractSampleVector<T>& anotherVector);
    ///
    /// \brief Поэлементное деление с элементами другого вектора
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator/=(const AbstractSampleVector<T>& anotherVector);
    ///
    /// \brief Умножение элементов на число
    /// \param[in] multiplier множитель
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator*=(const T& multiplier);
    ///
    /// \brief Деление элементов на число
    /// \param[in] divider делитель
    /// \return ссылка на текущий вектор
    ///
    AbstractSampleVector<T>& operator/=(const T& divider);

    ///
    /// \brief Заполняет вектор Гаусовским шумом (ничего не делает для комплексных векторов)
    /// \param[in] mean математическое ожидание
    /// \param[in] stddev среднеквадратичное отклонение
    ///
    virtual void fillGaussNoise(const T& mean, const T& stddev);
    ///
    /// \brief Заполняет вектор синусоидальным сигналом(тоном)
    /// \details Samples[i] = magnitude * cos(2πi*frequency + phase), 0 ≤ i < size
    /// \param[in] magnitude магнитуда, должна быть положительной
    /// \param[in] frequency частота тона относительно частоты дискретизации, должна быть в интервале [0.0, 0.5)
    /// \param[in,out] phase указатель на фазу тона относительно косинусной волны.
    /// Фаза должна находится в диапазоне [0,0, 2π). Вы можете использовать возвращаемое
    /// значение для вычисления следующего непрерывного блока данных.
    ///
    void fillTone(const double& magnitude, const double& frequency, double* phase);
    ///
    /// \brief Заполняет вектор сигналом Jaehne
    /// \details Samples[i]=magnitude*sin(0.5*π*i^2/size), 0 ≤ i < size.
    /// \param[in] magnitude магнитуда, должна быть положительной
    ///
    void fillJaehne(const double& magnitude);
    ///
    /// \brief Заполняет вектор линейно изменяющимися значениями(уклон)
    /// \details Samples[i]=offset + slope*i, 0 ≤ i < size.
    /// \param[in] offset Постоянная составляющая
    /// \param[in] slope Коэффициент уклона
    ///
    void fillRamp(const double& offset, const double& slope);
    ///
    /// \brief Заполняет вектор треугольным сигналом
    /// \details Samples[i] = magnitude * cth(2πi*frequency + phase), 0 ≤ i < len.
    /// \param[in] magnitude магнитуда, должна быть положительная
    /// \param[in] frequency частота треугольников относительно частоты дискретизации.
    /// Должна быть в интервале [0.0, 0.5)
    /// \param[in] asymetry асимметрия треугольника в диапазоне [-π, π).
    /// \param[in,out] phase указатель на фазу треугольника относительно косинусной волны.
    /// Фаза должна находится находиться в диапазоне [0,0, 2π). Вы можете использовать возвращаемое
    /// значение для вычисления следующего непрерывного блока данных.
    ///
    void fillTriangle(const double& magnitude, const double& frequency, const double& asymetry,
                      double* phase);

    void fillZeroes(std::size_t offset = 0, std::size_t count = 0) override;

    ///
    /// \brief Меняет значения бОльшие предельного значения на предельное значение
    /// \param[in] highValue верхнее предельное значение
    ///
    void limitHigh(const double& highValue);
    ///
    /// \brief Меняет значения бОльшие предельного значения на заданное
    /// \param[in] level верхнее предельное значение
    /// \param[in] value заданное значение
    ///
    void limitHigh(const double& level, const double& value);
    ///
    /// \brief Меняет значения меньшие предельного значения на предельное значение
    /// \param[in] lowValue нижнее предельное значение
    ///
    void limitLow(const double& lowValue);
    ///
    /// \brief Меняет значения меньшие предельного значения на заданное
    /// \param[in] level нижнее предельное значение
    /// \param[in] value заданное значение
    ///
    void limitLow(const double& level, const double& value);
    ///
    /// \brief Одновременное лимитирование значений вектора по нижней и верхней границе значений
    /// \param[in] lowValue нижнее предельное значение
    /// \param[in] highValue верхнее предельное значение
    ///
    void limitLowHigh(const double& lowValue, const double& highValue);
    ///
    /// \brief Лимитирование абсолютных значений вектора по нижней границе
    /// \param lowValue нижнее предельное значение
    ///
    void limitAbsLow(const double& lowValue);

    ///
    /// \brief Сырой массив данных
    /// \return указатель на массив отсчётов
    ///
    T* getSamples()
    {
        return _samples;
    }
    ///
    /// \brief Сырой константный массив данных
    /// \return указатель на массив отсчётов
    ///
    const T* getConstSamples() const
    {
        return _samples;
    }
    ///
    /// \brief Суммирует все элементы вектора
    /// \return сумма
    ///
    T sumUp() const
    {
        return sumUp(0, getSize());
    }
    ///
    /// \brief Суммирует диапазон элементов вектора
    /// \param[in] offset смещение от начала вектора
    /// \param[in] count количество элементов
    /// \return сумма
    ///
    T sumUp(int offset, int count) const;

    virtual QByteArray toQByteArray(char delimeter = ',') const override;
    virtual QByteArray toBinaryQByteArray(bool isInverseOrder = false) const override;

    bool isZero(const std::size_t index) const override;
    Complex64 complexAt(const std::size_t index) const override;
protected:
    ///
    /// \brief Преобразует значение элемента вектора в текст
    /// \param[in] idx индекс элемента
    /// \return строка
    ///
    virtual QString toString(std::size_t idx) const = 0;
    ///
    /// \brief Количество(максимальное) символов в текстовом представлении значения элемента вектора
    /// \return количество символов
    ///
    virtual int symbolsInTextValue() const = 0;

    virtual void inputValue(std::size_t idx, std::ifstream& stream);
    virtual void outputValue(std::size_t idx, std::ofstream& stream) const;
    ///
    /// \brief Дополнительная обработка массива сэмплов
    /// \param[in] startIdx индекс начала изменённой области
    /// \param[in] length длина изменённой области
    ///
    virtual void doCustomProcessing(const std::size_t startIdx = 0,
                                    const std::size_t length = 0)
    {
        Q_UNUSED(startIdx);
        Q_UNUSED(length);
    }

    ///
    /// \brief Удаляет выделенную под сэмплы память
    ///
    void deleteSamples();
    ///
    /// \brief Выделяет память под сэмплы
    ///
    void allocateSamples();

    ///
    /// \brief Блок памяти для значений вектора
    ///
    T* _samples;

    using AbstractVector::_valueUnit;
};

}
}
