#pragma once
#include "DspGlobal.h"
#include "DiscreteFourierTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс прямого ДПФ
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT DirectDft : public DiscreteFourierTransform<T>
{
    typedef DiscreteFourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструтор
    /// \param[in] size размер преобразования
    ///
    DirectDft(std::size_t size): _Parent(size) {}

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    using _Parent::_context;
};

///
/// \brief Класс прямого ДПФ одинарной точности
/// \ingroup Transforms
///
typedef DirectDft<float> DirectDft32;
///
/// \brief Класс прямого ДПФ двойной точности
/// \ingroup Transforms
///
typedef DirectDft<double> DirectDft64;

}
}
