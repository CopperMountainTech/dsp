#pragma once
#include "DspGlobal.h"
#include "AbstractTransform.h"

namespace Planar {
namespace Dsp {
///
/// \brief Базовый шаблонный класс преобразований Фурье
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT FourierTransform : public AbstractTransform<T>
{
    typedef AbstractTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер преобразования
    ///
    FourierTransform(std::size_t size);
    ~FourierTransform();

    virtual AbstractTransform<T>& operator()(AbstractVector* source, AbstractVector* result,
                                             const std::size_t num = 0,
                                             const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0) = 0;

    using _Parent::_size;
    using _Parent::_type;
};

typedef FourierTransform<float> FourierTransform32;
typedef FourierTransform<double> FourierTransform64;

}
}
