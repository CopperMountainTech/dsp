#pragma once
#include "IntVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Класс векторов целых беззнаковых 1-байтовых чисел (для буферов)
/// \ingroup Vectors
///
typedef IntVector<unsigned char> UCharVector;

}
}
