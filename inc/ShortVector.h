#pragma once
#include "IntVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Класс векторов целых знаковых 2-байтовых чисел(для данных с АЦП)
/// \ingroup Vectors
///
typedef IntVector<signed short> ShortVector;

}
}
