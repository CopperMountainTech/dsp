#pragma once
#include "DspGlobal.h"
#include "AbstractSampleVector.h"
#include "RealVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс векторов комплексных чисел. Доступные аргументы шаблона: float, double
/// \ingroup Vectors
///
template<class T>
class DSPSHARED_EXPORT ComplexVector : public AbstractSampleVector<Complex<T>>
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    ComplexVector(const std::size_t size);
    ///
    /// \brief Конструктор с инициализацией
    /// \param[in] size размер вектора
    /// \param[in] initValue значение для инициализации
    ///
    ComplexVector(const std::size_t size, const Complex<T>& initValue);
    ///
    /// \brief Конструктор с инициализацией из вещественного вектора
    /// \param[in] realPart вещественный вектор - действительная часть
    ///
    ComplexVector(const RealVector<T>& realPart);
    ///
    /// \brief Конструктор с инициализацией из 2х вещественных векторов
    /// \param[in] realPart действительная часть
    /// \param[in] imagPart мнимая часть
    ///
    ComplexVector(const RealVector<T>& realPart, const RealVector<T>& imagPart);
    ///
    /// \brief Конструктор с инициализацией из вещественного вектора умноженного на комплексную экспоненту.
    /// \details Данный вид конструтора удобно использовать для преобразования фильтра нижних частот в
    /// полосовой фильтр или для переноса сигнала на другую частоту
    /// \param[in] realVector вещественный вектор ФНЧ или сигнала
    /// \param[in] Fs частота оцифровки
    /// \param[in] Fc частота переноса
    ///
    ComplexVector(const RealVector<T>& realVector, double Fs, double Fc);
    ///
    /// \brief Конструктор копий
    /// \param[in] source исходный вектор
    ///
    ComplexVector(const ComplexVector<T>& source);
    ///
    /// \brief Деструктор
    ///
    virtual ~ComplexVector();

    ///
    /// \brief Поэлементное умножение с элементами другого вектора.
    /// Может применяться для наложения измерительного окна.
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    ComplexVector<T>& operator*=(const RealVector<T>& anotherVector);
    ///
    /// \brief Поэлементное умножение с элементами другого вектора
    /// \param[in] anotherVector другой вектор
    /// \return ссылка на текущий вектор
    ///
    ComplexVector<T>& operator*=(const ComplexVector<T>& anotherVector);

    ///
    /// \brief Конвертирует вектор вещественных чисел в вектор комплексных чисел
    /// \param[in] src конвертируемый вектор
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] dstOffset смещение в конечном векторе
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void copyFromRealVector(const RealVector<T>* src, const std::size_t srcOffset = 0,
                            const std::size_t dstOffset = 0, const std::size_t count = 0);

    virtual void copyFrom(const AbstractVector* anotherVector, const std::size_t srcOffset = 0,
                          const std::size_t dstOffset = 0, const std::size_t count = 0) override;
    ///
    /// \brief Апроксимированное значение в точке
    /// \param[in] x запрашиваемая точка
    /// \return апроксимированное значение
    ///
    Complex<T> getApproximateValue(const T x) const;
    ///
    /// \brief Апроксимированное значение в точке с единицами измерения
    /// \param[in] argument значение аргумента с единицами измерения
    /// \param[out] resultRe действительная часть апроксимированного значения с единицами измерения
    /// \param[out] resultIm мнимая часть апроксимированного значения с единицами измерения
    ///
    void getApproximateUnitValue(const Unit& argument, Unit& resultRe, Unit& resultIm) const;
    ///
    /// \brief Заполняет комлексный вектор действительная и мнимая части которого представлены отдельными
    /// вещественными векторами
    /// \param[in] realPart действительная часть
    /// \param[in] imagPart мнимая часть
    ///
    void combineIQ(const RealVector<T>& realPart, const RealVector<T>& imagPart);
    ///
    /// \brief Разбивает вектор на действительную и мнимые части
    /// \param[out] realPart действительная часть
    /// \param[out] imagPart мнимая часть
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void splitIQ(RealVector<T>& realPart, RealVector<T>& imagPart,
                 const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Разбивает вектор на действительную и мнимые части
    /// \param[out] realPart действительная часть
    /// \param[out] imagPart мнимая часть
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void splitIQ(T* realPart, T* imagPart,
                 const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Копирует действительную часть вектора в действительный вектор
    /// \param[out] realPart выходной действительный вектор
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void copyReal(RealVector<T>& realPart,
                  const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Копирует мнимую часть вектора в действительный вектор
    /// \param[out] imaginaryPart выходной действительный вектор
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void copyImaginary(RealVector<T>& imaginaryPart,
                       const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Вычисляет фазы в радианах элементов комплексного вектора
    /// (Phase values are in the range (-π, π])
    /// \param phaseVector[out] выходной вектор фаз
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void computePhase(RealVector<T>& phaseVector,
                      const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Вычисляет магнитуды элементов комплексного вектора
    /// \param magnitudeVector[out] выходной вектор магнитуд
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] count количество значений для копирования. если 0, то все значения до конца вектора
    ///
    void computeMagnitude(RealVector<T>& magnitudeVector,
                          const std::size_t srcOffset = 0, const std::size_t count = 0) const;
    ///
    /// \brief Вычисляет спектр мощности комплексного вектора в диапазоне [begin, end_[
    /// \param[out] destination ссылка на результирующий действительный вектор
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    ///
    void getPowerSpectrum(RealVector<T>& destination, const std::size_t begin,
                          const std::size_t end_) const;
    ///
    /// \brief Вычисляет спектр мощности комплексного вектора
    /// \param[out] destination ссылка на результирующий действительный вектор
    ///
    void getPowerSpectrum(RealVector<T>& destination) const
    {
        getPowerSpectrum(destination, 0, this->getSize());
    }

    virtual void fillGaussNoise(const T& mean, const T& stddev);

    ///
    /// \brief Умножение элементов на действительное число
    /// \param[in] multiplier действительный множитель
    /// \return ссылка на текущий вектор
    ///
    ComplexVector<T>& operator*=(const T& multiplier);

    virtual void setScaleUnit(const Unit& valueUnit, bool rescaleRequired = false) override;
    ///
    /// \brief Значение действительной части элемента вектора с еденицами измерения
    /// \param[in] i индекс элемента вектора
    /// \return объект Unit
    ///
    Unit realAsUnit(const std::size_t i) const;
    ///
    /// \brief Значение мнимой части элемента вектора с еденицами измерения
    /// \param[in] i индекс элемента вектора
    /// \return объект Unit
    ///
    Unit imagAsUnit(const std::size_t i) const;
protected:
    virtual QString toString(std::size_t idx) const;
    virtual int symbolsInTextValue() const;
    virtual void inputValue(std::size_t idx, std::ifstream& stream);
    virtual void outputValue(std::size_t idx, std::ofstream& stream) const;

    using AbstractSampleVector<Complex<T>>::_samples;
    using AbstractSampleVector<Complex<T>>::_type;
    using AbstractSampleVector<Complex<T>>::_valueUnit;
    typedef AbstractSampleVector<Complex<T>> BaseType;
};

///
/// \brief Вектор комплексных чисел одинарной точности
/// \ingroup Vectors
///
typedef ComplexVector<float> ComplexVector32;
///
/// \brief Вектор комплексных чисел двойной точности
/// \ingroup Vectors
///
typedef ComplexVector<double> ComplexVector64;
}
}
