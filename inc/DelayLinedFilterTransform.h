#pragma once
#include "FilterTransform.h"

namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс фильтрации с заполненной начальными значениями линии задержки
/// \details В классе FilterTransform первые результирующие значения получаются из наложения
/// фильтра на первый элемент входной последовательности отсчётов и линии задержки, которая заполняется
/// нулями. Получается некотрый переходный процесс.
/// Данный класс заполняет линию задержки начальными значениями входной последовательности и сдвигает
/// начало этой последовательности на длинну линии задержки. Таким образом первое итоговое значение получается
/// из наложения фильтра только на значения входной последовательности.
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT DelayLinedFilterTransform : public FilterTransform<T>
{
    typedef FilterTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    /// \param[in] taps вектор хранящий в себе значения фильтров
    /// \param[in] downFactor коэффициент понижения дискретизации
    /// \param[in] upFactor коэффициент повышения дискретизации
    ///
    DelayLinedFilterTransform(const ComplexVector<T>* taps, int downFactor = 1, int upFactor = 1);
    ~DelayLinedFilterTransform();

    /*private:

        Dsp::ComplexVector<T>* _srcDelayLine;*/
protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    using _Parent::_size;
    using _Parent::_context;
};

///
/// \brief Класс фильтрации с заполненной начальными значениями линии задержки одинарной точности
/// \ingroup Transforms
///
typedef DelayLinedFilterTransform<float> DelayLinedFilterTransform32;
///
/// \brief Класс фильтрации с заполненной начальными значениями линии задержки двойной точности
/// \ingroup Transforms
///
typedef DelayLinedFilterTransform<double> DelayLinedFilterTransform64;

}
}
