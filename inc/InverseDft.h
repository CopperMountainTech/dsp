#pragma once
#include "DspGlobal.h"
#include "DiscreteFourierTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс обратного ДПФ
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT InverseDft : public DiscreteFourierTransform<T>
{
    typedef DiscreteFourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструтор
    /// \param[in] size размер преобразования
    ///
    InverseDft(std::size_t size): _Parent(size) {}

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    using _Parent::_context;
};

///
/// \brief Класс обратного ДПФ одинарной точности
/// \ingroup Transforms
///
typedef InverseDft<float> InverseDft32;
///
/// \brief Класс обратного ДПФ двойной точности
/// \ingroup Transforms
///
typedef InverseDft<double> InverseDft64;

}
}
