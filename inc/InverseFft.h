#pragma once
#include "DspGlobal.h"
#include "FastFourierTransform.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {
///
/// \brief Шаблонный класс обратного БПФ по основанию 2^n
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT InverseFft : public FastFourierTransform<T>
{
    typedef FastFourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструтор
    /// \param[in] order порядок степени 2
    ///
    InverseFft(int order): _Parent(order) { }

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0);

    using _Parent::_context;
};

///
/// \brief Класс обратного БПФ одинарной точности
/// \ingroup Transforms
///
typedef InverseFft<float> InverseFft32;
///
/// \brief Класс обратного БПФ двойной точности
/// \ingroup Transforms
///
typedef InverseFft<double> InverseFft64;

}
}
