#pragma once

#include "DspGlobal.h"
#include <MathEssentials.h>
#include <Units.h>
#include <vector>
#include <functional>

namespace Planar {
namespace Dsp {
///
/// \brief Абстрактный класс для абсцисс векторов.
/// \details Является базовым (интерфейсным) классом для конкретных классов абсцисс
/// \ingroup Markups
///
class AbstractAbscissa
{
public:
    ///
    /// \brief Типы абсцисс
    ///
    enum AbscissaType {
        UniType = 0,
        RndType,
        SimpleType
    };

    ///
    /// \brief Конструктор
    /// \param[in] size размер абсциссы
    ///
    AbstractAbscissa(const std::size_t size)
    {
        _maxIndex = size - 1;
        _scaleUnit.setValue(1);
    }

    ///
    /// \brief Деструктор
    ///
    virtual ~AbstractAbscissa()
    {
    }

    ///
    /// \brief Тип абсциссы
    /// \return одно из значений enum AbscissaType
    ///
    int getType() const
    {
        return _type;
    }
    ///
    /// \brief Геттер размера абсциссы
    /// \return размер абсциссы
    ///
    int getSize()
    {
        return int(_maxIndex + 1);
    }

    ///
    /// \return Значение индекса разметки по горизонтальной оси, не выходящее за пределы значений индекса
    /// \param[in] n требуемый индекс
    ///
    std::size_t getSafeIndex(const std::size_t n) const
    {
        if (n > _maxIndex) return _maxIndex;
        return n;
    }

    ///
    /// \return значение X для индекса
    /// \param[in] index индекс
    ///
    virtual double getValue(const std::size_t index) const = 0;

    ///
    /// \brief Устанавливает новое значение X для индекса
    /// \param[in] index индекс
    /// \param[in] newValue значение
    ///
    virtual void setValue(const std::size_t index, const double newValue) = 0;

    ///
    /// \return минимальное значение разметки по горизонтальной оси
    ///
    virtual double getMin() const = 0;

    ///
    /// \return максимальное значение разметки по горизонтальной оси
    ///
    virtual double getMax() const = 0;

    ///
    /// \return индекс значения, большего либо равного заданному
    /// \param[in] x заданное значение
    ///
    virtual std::size_t getCeilIndex(const double x) const = 0;

    ///
    /// \return индекс значения, меньшего либо равного заданному
    /// \param[in] x заданное значение
    ///
    virtual std::size_t getFloorIndex(const double x) const = 0;

    ///
    /// \return индекс значения, ближайшего заданному
    /// \param[in] x заданное значение
    ///
    virtual std::size_t getNearIndex(const double x) const = 0;

    ///
    /// \return индекс значения, меньшего либо равного заданному, или максимальный индекс - 1
    /// \param[in] x заданное значение
    ///
    std::size_t getLineIndex(const double x) const
    {
        std::size_t ix = getFloorIndex(x);
        return ix < _maxIndex ? ix : _maxIndex - 1;
    }
    ///
    /// \brief Создаёт копию текущей разметки
    /// \return указатель на новую разметку
    ///
    virtual AbstractAbscissa* createCopy() const = 0;

    ///
    /// \brief Единицы измерения элементов абсциссы
    /// \return объект Unit
    ///
    Unit scaleUnit() const
    {
        return _scaleUnit;
    }
    ///
    /// \brief Задаёт единицы измерения элементов абсциссы
    /// \param[in] scaleUnit объект Unit
    /// \param[in] rescaleRequired если true, то значения элементов абсциссы
    /// изменятся согласно новым единицам измерений
    ///
    void setScaleUnit(const Unit& scaleUnit, bool rescaleRequired = false)
    {
        if (rescaleRequired)
            rescaleToUnit(scaleUnit);
        _scaleUnit = scaleUnit;
        _scaleUnit.setValue(1);
    }
    ///
    /// \brief Значение абсциссы с единицами измерения
    /// \param[in] index индекс элемента абсциссы
    /// \return объект Unit
    ///
    Unit valueAsUnit(const std::size_t index) const
    {
        Unit unit = _scaleUnit;
        unit.setValue(getValue(index));
        return unit;
    }

protected:
    virtual void rescaleToUnit(const Unit& newUnit) = 0;
    ///
    /// \brief _maxIndex максимальный индекс
    ///
    std::size_t _maxIndex;
    ///
    /// \brief Юнит шкалы абсциссы
    ///
    Unit _scaleUnit;
    ///
    /// \brief Тип абсциссы
    ///
    int _type;
};

// ---------------------------- UniAbscissa --------------------------------------------------

///
/// \brief Линейная абсцисса
/// \ingroup Markups
///
class DSPSHARED_EXPORT UniAbscissa : public AbstractAbscissa
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер абсциссы
    /// \param[in] x0 начальное значение
    /// \param[in] step разница между соседними значениями абсциссы
    ///
    UniAbscissa(const std::size_t size, const double x0, const double step) :
        AbstractAbscissa(size),
        _x0(x0),
        _xStep(step)
    {
        _type = UniType;
    }

    double getMin()  const
    {
        return _x0;
    }

    double getMax() const
    {
        return _x0 + _xStep * _maxIndex;
    }

    double getStep() const
    {
        return _xStep;
    }

    double getValue(const std::size_t index) const
    {
        return _x0 + _xStep * index;
    }

    ///
    /// \brief Задание значения абсциссы - пораждает исключительную ситуацию,
    /// так как для этого типа абсцисс нельзя использовать прямое задание значения.
    /// \param[in] index индекс
    /// \param[in] newValue новое значение
    ///
    void setValue(const std::size_t index, const double newValue);

    std::size_t getCeilIndex(const double x) const
    {
        return getSafeIndex(std::size_t(ceil((x - _x0) / _xStep)));
    }

    std::size_t getFloorIndex(const double x) const
    {
        return getSafeIndex(std::size_t(floor((x - _x0) / _xStep)));
    }

    std::size_t getNearIndex(const double x) const
    {
        return getSafeIndex(std::size_t(Planar::intRound((x - _x0) / _xStep)));
    }
    AbstractAbscissa* createCopy() const
    {
        AbstractAbscissa* newAbscissa = new UniAbscissa(_maxIndex + 1, _x0, _xStep);
        newAbscissa->setScaleUnit(_scaleUnit);
        return newAbscissa;
    }
protected:
    virtual void rescaleToUnit(const Unit& newUnit)
    {
        double multiplier = _scaleUnit.multiplier() / newUnit.multiplier();
        _x0 *= multiplier;
        _xStep *= multiplier;

    }

private:
    ///
    /// \brief начальное значение абсциссы
    ///
    double _x0;
    ///
    /// \brief разница между соседними значениями абсциссы
    ///
    double _xStep;

};

// ------------------------------- RndAbscissa --------------------------------------------

///
/// \brief Произвольная абцисса
/// \ingroup Markups
///
class DSPSHARED_EXPORT RndAbscissa : public AbstractAbscissa
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер абсциссы
    ///
    RndAbscissa(const std::size_t size) :
        AbstractAbscissa(size),
        _values(size)
    {
        _type = RndType;
    }

    double getMin() const
    {
        return _values[0];
    }

    double getMax() const
    {
        return _values[_maxIndex];
    }

    double getValue(const std::size_t index) const
    {
        return _values[getSafeIndex(index)];
    }

    void setValue(const std::size_t index, const double newValue)
    {
        _values[index] = newValue;
    }

    std::size_t getCeilIndex(const double x) const;
    std::size_t getFloorIndex(const double x) const;
    std::size_t getNearIndex(const double x) const;
    AbstractAbscissa* createCopy() const
    {
        auto newAbscissa = new RndAbscissa(_values.size());
        newAbscissa->setScaleUnit(_scaleUnit);
        newAbscissa->_values = _values;
        return newAbscissa;
    }
protected:
    virtual void rescaleToUnit(const Unit& newUnit)
    {
        double multiplier = _scaleUnit.multiplier() / newUnit.multiplier();
        std::transform(_values.begin(), _values.end(), _values.begin(),
                       std::bind1st(std::multiplies<double>(), multiplier));
    }
private:
    ///
    /// \brief вектор значений
    ///
    std::vector<double> _values;//NOTE: а дабл ли тут нужен?
};

// -------------------------- SimpleAbscissa -----------------------------------

///
/// \brief Простая абсцисса - индекс равен значению
/// \ingroup Markups
///
class DSPSHARED_EXPORT SimpleAbscissa : public AbstractAbscissa
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер абсциссы
    ///
    SimpleAbscissa(const std::size_t size) : AbstractAbscissa(size)
    {
        _type = SimpleType;
    }

    double getMin() const
    {
        return 0;
    }

    double getMax() const
    {
        return _maxIndex;
    }

    double getValue(const std::size_t index) const
    {
        return getSafeIndex(index);
    }

    ///
    /// \brief Задание значения абсциссы - пораждает исключительную ситуацию,
    /// так как для этого типа абсцисс нельзя использовать прямое задание значения.
    /// \param[in] index индекс
    /// \param[in] newValue новое значение
    ///
    void setValue(const std::size_t index, const double newValue);

    std::size_t getCeilIndex(const double x) const
    {
        return getSafeIndex(std::size_t(ceil(x)));
    }

    std::size_t getFloorIndex(const double x) const
    {
        return getSafeIndex(std::size_t(floor(x)));
    }

    std::size_t getNearIndex(const double x) const
    {
        return getSafeIndex(std::size_t(Planar::intRound(x)));
    }
    AbstractAbscissa* createCopy() const
    {
        AbstractAbscissa* newAbscissa = new SimpleAbscissa(_maxIndex + 1);
        newAbscissa->setScaleUnit(_scaleUnit);
        return newAbscissa;
    }
protected:
    virtual void rescaleToUnit(const Unit& newUnit);
};

}//namespace Dsp
}//namespace Planar
