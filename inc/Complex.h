#pragma once
#include <complex>

#include <MathEssentials.h>

namespace Planar {
namespace Dsp {

///
/// \brief Коплексное число одинарной точности
/// \ingroup Complex
///
typedef std::complex<float> Complex32;
///
/// \brief Комплексное число двойной точности
/// \ingroup Complex
///
typedef std::complex<double> Complex64;
///
/// \brief Комплексное число
/// \ingroup Complex
///
template<class T> using Complex = std::complex<T>;

///
/// \brief Комплексный ноль
/// \ingroup Complex
///
template<class T> constexpr Complex<T> complexZero();
static Complex32 (&complexZero32)() = complexZero<float>;
static Complex64 (&complexZero64)() = complexZero<double>;

///
/// \brief Комплексная единица
/// \ingroup Complex
///
template<class T> constexpr Complex<T> complexOne();
static Complex32 (&complexOne32)() = complexOne<float>;
static Complex64 (&complexOne64)() = complexOne<double>;

///
/// \brief Комплексное i
/// \ingroup Complex
///
template<class T> constexpr Complex<T> complexJ();
static Complex32 (&complexJ32)() = complexJ<float>;
static Complex64 (&complexJ64)() = complexJ<double>;

///
/// \param[in] value комплексное число
/// \return Истина, если комплексное число равно 0
/// \ingroup Complex
///
template<class T> inline bool isZero(const Complex<T> value);

///
/// \brief Нахождение промежуточного комплексного значения между двумя заданными
/// \param[in] factor доля "расстояния" от первого числа до искомого
/// \param[in] value1 первое число
/// \param[in] value2 второе число
/// \return промежуточное комплексное значение
/// \ingroup Complex
///
template<class T>
Complex<T> complexInterpolate(const T factor, const Complex<T> value1, const Complex<T> value2);


//--------------------------------------------------------------------------------------------------


template<class T> constexpr Complex<T> complexZero()
{
    return std::complex<T>(0, 0);
}

template<class T> constexpr Complex<T> complexOne()
{
    return std::complex<T>(1, 0);
}

template<class T> constexpr Complex<T> complexJ()
{
    return std::complex<T>(0, 1);
}

template<class T> inline bool isZero(const Complex<T> value)
{
    return Planar::isZero(value.real()) && Planar::isZero(value.imag());
}

template<class T>
Complex<T> complexInterpolate(const T factor, const Complex<T> value1, const Complex<T> value2)
{
    if (isZero(value1) || isZero(value2))
        return value1 + factor * (value2 - value1);

    T r0 = std::abs(value1);
    T a0 = std::arg(value1);
    T r1 = std::abs(value2);
    T a1 = std::arg(value2);
    T dr=(r1 - r0);
    T da=T(Planar::normalizeRadianPhase(a1 - a0));

    return std::polar(r0 + factor * dr, a0 + factor * da);
}

}
}
