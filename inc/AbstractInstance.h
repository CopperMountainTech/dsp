#pragma once
#include "DspGlobal.h"

namespace Planar {
namespace Dsp {

///
/// \brief Абстрактная сущность
/// \details Базовый класс для векторов, преобразований. Имеет некоторый размер и занимает
/// некоторое количество байт в памяти
/// \ingroup Essentials
///
class AbstractInstance
{
public:
    AbstractInstance(std::size_t size = 0): _size(size) {}
    virtual ~AbstractInstance() {}
    ///
    /// \brief Возвращает размер объекта
    /// \return размер
    ///
    virtual std::size_t getSize() const
    {
        return _size;
    }
    ///
    /// \brief Возвращает количество выделенной для объекта памяти в байтах
    /// \return количество байт
    ///
    virtual std::size_t getBytesCount() const = 0;
protected:
    ////
    /// \brief Задаёт размер объекта
    /// \param size размер в единицах
    ///
    void setSize(std::size_t size)
    {
        _size = size;
    }

    std::size_t _size;
};

}
}
