﻿#pragma once
#include "DspGlobal.h"
#include "AbstractSampleVector.h"
#include "ShortVector.h"

namespace Planar {
namespace Dsp {

///
/// \brief Шаблонный класс векторов действительных чисел. Доступные аргументы шаблона: float, double
/// \ingroup Vectors
///
template<class T>
class DSPSHARED_EXPORT RealVector : public AbstractSampleVector<T>
{
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер вектора
    ///
    RealVector(const std::size_t size);
    ///
    /// \brief Конструктор с инициализацией
    /// \param[in] size размер вектора
    /// \param[in] initValue значение для инициализации
    ///
    RealVector(const std::size_t size, const T& initValue);
    ///
    /// \brief Конструктор копий
    /// \param[in] source исходный вектор
    ///
    RealVector(const RealVector<T>& source);
    ///
    /// \brief Деструктор
    ///
    virtual ~RealVector();

    ///
    /// \brief Конвертирует вектор целых 2байтовых чисел в вещественный вектор.
    /// Может использоваться для конвертирования данных с АЦП
    /// \param[in] shortvector вектор целых 2байтовых чисел
    /// \param[in] srcOffset смещение исходного вектора
    /// \param[in] dstOffset смещение в конечном векторе
    /// \param[in] count количество значений для конвертации. если 0, то все значения до конца исходного вектора
    ///
    void copyFromShortVector(const ShortVector* shortvector, const std::size_t srcOffset = 0,
                             const std::size_t dstOffset = 0, const std::size_t count = 0);

    virtual void copyFrom(const AbstractVector* anotherVector, const std::size_t srcOffset = 0,
                          const std::size_t dstOffset = 0, const std::size_t count = 0) override;
    ///
    /// \brief Заполняет вектор значениями абсциссы другого вектора
    /// \param[in] anotherVector вектор с абсциссой
    ///
    void copyFromVectorAbscissa(const AbstractVector& anotherVector);
    ///
    /// \brief Заполняет вектор значениями абсциссы
    /// \param[in] abscissa абсцисса
    ///
    void copyFromAbscissa(AbstractAbscissa* abscissa);
    ///
    /// \brief Апроксимированное значение в точке
    /// \param[in] x запрашиваемая точка
    /// \return апроксимированное значение
    ///
    T getApproximateValue(const T x) const;
    ///
    /// \brief Апроксимированное значение в точке с единицами измерения
    /// \param[in] argument значение аргумента с единицами измерения
    /// \param[out] result апроксимированное значения с единицами измерения
    ///
    void getApproximateUnitValue(const Unit& argument, Unit& result) const;
    ///
    /// \brief Прореживает вектор
    /// \param[out] dst результирующий вектор
    ///
    void decimateTo(RealVector<T>& dst);

    ///
    /// \brief Вычисляет натуральный логарифм каждого элемента вектора
    ///
    void ln();
    ///
    /// \brief Вычисляет натуральный логарифм каждого элемента вектора
    /// \param[out] destination ссылка на результирующий вектор
    ///
    void lnTo(RealVector<T>& destination) const;
    ///
    /// \brief Вычисляет десятичный логарифм каждого элемента вектора
    ///
    void log10();
    ///
    /// \brief Вычисляет десятичный логарифм каждого элемента вектора
    /// \param[out] destination ссылка на результирующий вектор
    ///
    void log10To(RealVector<T>& destination) const;
    ///
    /// \brief Вычисляет абсолютное значение каждого элемента вектора
    ///
    void abs();
    ///
    /// \brief Вычисляет косинус каждого элемента ветора
    /// \param[out] destination ссылка на результирующий вектор
    ///
    void cosTo(RealVector<T>& destination);

    ///
    /// \brief Средняя мощность элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \return значение средней мощности
    ///
    T getAveragePower(const std::size_t begin, const std::size_t end_) const;

    ///
    /// \brief Средняя мощность элементов вектора
    /// \return значение средней мощности
    ///
    T getAveragePower() const
    {
        return getAveragePower(0, this->getSize());
    }

    ///
    /// \brief Арифметическое среднее элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \return значение среднего
    ///
    T getAverage(const std::size_t begin, const std::size_t end_) const;

    ///
    /// \brief Арифметическое среднее элементов вектора
    /// \return значение среднего
    ///
    T getAverage() const
    {
        return getAverage(0, this->getSize());
    }

    ///
    /// \brief Минимальное значение элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \return минимальное значение
    ///
    T getMin(const std::size_t begin, const std::size_t end_) const;

    ///
    /// \brief Минимальное значение элементов вектора
    /// \return минимальное значение
    ///
    T getMin() const
    {
        return getMin(0, this->getSize());
    }

    ///
    /// \brief Максимальное значение элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \return максимальное значение
    ///
    T getMax(const std::size_t begin, const std::size_t end_) const;

    ///
    /// \brief Максимальное значение элементов вектора
    /// \return максимальное значение
    ///
    T getMax() const
    {
        return getMax(0, this->getSize());
    }

    ///
    /// \brief Максимальное и минимальное значения элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \param[out] pMin ссылка на минимальное значение
    /// \param[out] pMax ссылка на максимальное значение
    ///
    void getMinMax(const std::size_t begin, const std::size_t end_, T& pMin, T& pMax) const;
    ///
    /// \brief Максимальное и минимальное значения элементов вектора в диапазоне [begin, end_[
    /// \param[in] begin начало диапазона
    /// \param[in] end_ конец диапазона
    /// \param[out] pMin ссылка на минимальное значение
    /// \param[out] pMinIdx индекс минимального значения
    /// \param[out] pMax ссылка на максимальное значение
    /// \param[out] pMaxIdx индекс максимального значения
    ///
    void getMinMaxIdx(const std::size_t begin, const std::size_t end_, T& pMin, std::size_t& pMinIdx,
                      T& pMax, std::size_t& pMaxIdx) const;

    ///
    /// \brief Максимальное и минимальное значения элементов вектора
    /// \param[out] pMin ссылка на минимальное значение
    /// \param[out] pMax ссылка на максимальное значение
    ///
    void getMinMax(T& pMin, T& pMax) const
    {
        getMinMax(0, this->getSize(), pMin, pMax);
    }
    ///
    /// \brief Максимальное и минимальное значения элементов вектора
    /// \param[out] pMin ссылка на минимальное значение
    /// \param[out] pMinIdx индекс минимального значения
    /// \param[out] pMax ссылка на максимальное значение
    /// \param[out] pMaxIdx индекс максимального значения
    ///
    void getMinMaxIdx(T& pMin, std::size_t& pMinIdx, T& pMax, std::size_t& pMaxIdx) const
    {
        getMinMaxIdx(0, this->getSize(), pMin, pMinIdx, pMax, pMaxIdx);
    }
    ///
    /// \brief заполняет вектор Гауссовским измерительным окном
    /// \param[in] alpha параметр измерительного окна
    ///
    void fillMeasureWindow(double alpha);

    virtual void setScaleUnit(const Unit& valueUnit, bool rescaleRequired = false) override;
    ///
    /// \brief Значение элемента вектора с еденицами измерения
    /// \param[in] i индекс элемента вектора
    /// \return объект Unit
    ///
    Unit valueAsUnit(const std::size_t i) const;
protected:
    virtual QString toString(std::size_t idx) const;
    virtual int symbolsInTextValue() const;
    using AbstractSampleVector<T>::_samples;
    using AbstractSampleVector<T>::_type;
    using AbstractSampleVector<T>::_valueUnit;
    typedef AbstractSampleVector<T> BaseType;
};

///
/// \brief Вектор действительных чисел одинарной точности
/// \ingroup Vectors
///
typedef RealVector<float> RealVector32;
///
/// \brief Вектор действительных чисел двойной точности
/// \ingroup Vectors
///
typedef RealVector<double> RealVector64;

}//namespace Dsp
}//namespace Planar
