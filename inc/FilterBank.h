#pragma once
#include "DspGlobal.h"
#include "RealVector.h"
#include "ComplexVector.h"

namespace Planar {
namespace Dsp {

//template<class T>
class DSPSHARED_EXPORT FilterBank
{
public:
    static RealVector64* generateRaisedCosine(int halfSize);
    static void generateRaisedCosine(RealVector64& destination, int halfSize);

};

} // namespace Dsp
} // namespace Planar
