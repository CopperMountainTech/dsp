#pragma once
#include "DspGlobal.h"
#include "FourierTransform.h"

namespace Planar {
namespace Dsp {
///
/// \brief Базовый шаблонный класс ДПФ
/// \ingroup Transforms
///
template<class T>
class DSPSHARED_EXPORT DiscreteFourierTransform : public FourierTransform<T>
{
    typedef FourierTransform<T> _Parent;
public:
    ///
    /// \brief Конструктор
    /// \param[in] size размер преобразования
    ///
    DiscreteFourierTransform(std::size_t size);
    ~DiscreteFourierTransform();

    std::size_t getBytesCount() const;
    ///
    /// \brief Расчитывает размер необходимого объёма памяти для некоторого количества ДПФ.
    /// \param[in] size размер ДПФ
    /// \param[in] appFactor коэффициент применимости - количество однотипных преобразований
    /// \return размер памяти в байтах
    ///
    static std::size_t calcNeededMemory(std::size_t size, unsigned int appFactor);

protected:
    virtual void run(ComplexVector<T>* source, ComplexVector<T>* result,
                     const std::size_t num = 0,
                     const std::size_t sourceOffset = 0, const std::size_t resultOffset = 0) = 0;

    using _Parent::_size;
    using _Parent::_context;
};

typedef DiscreteFourierTransform<float> DiscreteFourierTransform32;
typedef DiscreteFourierTransform<double> DiscreteFourierTransform64;

}
}
