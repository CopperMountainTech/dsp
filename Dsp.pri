message("Dsp in " $$PWD)

LIB_ARCH = x64
contains(QMAKE_HOST.arch, x86):LIB_ARCH = x32

win32:contains(QMAKE_HOST.os, Linux):LIB_ARCH = x32

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/$$LIB_ARCH -lDSP
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/$$LIB_ARCH -lDSPd
else:unix: LIBS += -L$$PWD/lib/$$LIB_ARCH -lDSP
INCLUDEPATH += $$PWD/inc/

DST_DIR=$$top_builddir

unix:!macx:COPY_FILES = $$PWD/lib/$$LIB_ARCH/*.so
win32:COPY_FILES = $$PWD/lib/$$LIB_ARCH/*.dll

contains(QMAKE_HOST.os,Windows): {
    COPY_FILES ~= s,/,\\,g
    DST_DIR ~= s,/,\\,g
}

QMAKE_POST_LINK += $$escape_expand(\n\t) $$QMAKE_COPY $$quote($$COPY_FILES) $$quote($$DST_DIR)

#include($$PWD/inc/Inc.pri)
include($$top_srcdir_libraries/Ipp/Ipp.pri)
